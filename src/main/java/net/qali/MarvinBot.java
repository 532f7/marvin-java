package net.qali;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import net.qali.plugins.Fortune;
import net.qali.plugins.ProcessBuilderWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.commands.SetMyCommands;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.commands.BotCommand;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class MarvinBot extends TelegramLongPollingBot {
  private static final String PROP_TOKEN = "token";
  private static final Logger logger = LoggerFactory.getLogger(TelegramLongPollingBot.class);
  private final Fortune fortunePlugin;

  public MarvinBot() throws IllegalArgumentException {
    super();
    fortunePlugin = new Fortune(new ProcessBuilderWrapper());
    if (System.getProperty(PROP_TOKEN) == null) {
      throw new IllegalArgumentException(PROP_TOKEN + " property is not set");
    }
  }

  public void onUpdateReceived(Update update) {
    // We check if the update has a message and the message has text
    if (update.hasMessage() && update.getMessage().hasText()) {
      // Set variables
      String messageText = update.getMessage().getText();

      if (!update.getMessage().isCommand()) {
        logger.info("skipping message: " + messageText);
        return;
      }

      if (fortunePlugin.shouldHandle(update.getMessage())) {
        Optional<String> fortune = fortunePlugin.handleMessage(update.getMessage());

        // TODO: this isn't really needed for fortune, but might be needed for other plugins
        if (fortune.isEmpty()) {
          logger.error("failed to get fortune");
          return;
        }

        sendMessage(update.getMessage(), fortune.get());
      }
    }
  }

  private void sendMessage(Message replyTo, String messageBody) {
    SendMessage message = new SendMessage().setChatId(replyTo.getChatId()).setText(messageBody);
    try {
      execute(message);
    } catch (TelegramApiException e) {
      logger.info("failed to reply to message " + replyTo.getText(), e);
    }
  }

  @Override
  public void onUpdatesReceived(List<Update> updates) {
    for (Update u : updates) {
      onUpdateReceived(u);
    }
  }

  @Override
  public String getBotUsername() {
    return "m4rv1nbot";
  }

  @Override
  public String getBotToken() {
    return System.getProperty(PROP_TOKEN);
  }

  public void setCommands() throws TelegramApiException {
    List<BotCommand> commands = new ArrayList<>();
    commands.add(fortunePlugin.getCommand());
    SetMyCommands setMyCommands = new SetMyCommands(commands);
    execute(setMyCommands);
  }
}
