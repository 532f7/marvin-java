package net.qali;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;

public class Main {
  private static final Logger logger = LoggerFactory.getLogger(Main.class);

  public static void main(String[] args) throws TelegramApiRequestException {
    ApiContextInitializer.init();
    TelegramBotsApi botsApi = new TelegramBotsApi("https://qali.net:8443", "http://127.0.0.1:8080");
    try {
      MarvinBot bot = new MarvinBot();
      botsApi.registerBot(bot);
      bot.setCommands();
    } catch (TelegramApiRequestException e) {
      logger.error("register failed", e);
    } catch (TelegramApiException e) {
      logger.error("bot failed", e);
    }
  }
}
