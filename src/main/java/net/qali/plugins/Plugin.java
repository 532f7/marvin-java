package net.qali.plugins;

import java.util.Optional;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.commands.BotCommand;

public interface Plugin {
  /** @return the BotCommand used to respond to this plugin */
  public BotCommand getCommand();

  /** Processes the given Message and returns an optional message to post in response */
  public Optional<String> handleMessage(Message message);

  /**
   * Returns whether or not this plugin should handle the given message
   *
   * @param message
   * @return
   */
  public boolean shouldHandle(Message message);
}
