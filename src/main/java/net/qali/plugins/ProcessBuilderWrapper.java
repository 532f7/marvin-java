package net.qali.plugins;

import java.io.IOException;
import java.util.List;

/**
 * Wrapper around ProcessBuilder for test purposes. only implements methods required for fortune
 * calls
 */
public class ProcessBuilderWrapper {
  private final ProcessBuilder processBuilder;

  public ProcessBuilderWrapper() {
    this.processBuilder = new ProcessBuilder();
  }

  public List<String> getCommand() {
    return processBuilder.command();
  }

  public void command(String... opts) {
    processBuilder.command(opts);
  }

  public Process start() throws IOException {
    return processBuilder.start();
  }
}
