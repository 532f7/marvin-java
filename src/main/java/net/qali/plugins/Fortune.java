package net.qali.plugins;

import java.io.IOException;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.commands.BotCommand;

public class Fortune implements Plugin {
  private static final Logger logger = LoggerFactory.getLogger(Fortune.class);

  private static final String CMD = "/fortune";
  private static final BotCommand command = new BotCommand(CMD, "fortune [fortune file]");

  private final ProcessBuilderWrapper processBuilder;

  public Fortune(ProcessBuilderWrapper processBuilder) {
    this.processBuilder = processBuilder;
  }

  @Override
  public BotCommand getCommand() {
    return command;
  }

  @Override
  public boolean shouldHandle(Message message) {
    return message.getText().startsWith(CMD);
  }

  @Override
  public Optional<String> handleMessage(Message message) {
    String[] tokens = message.getText().split(" ");
    if (tokens.length > 2) {
      return Optional.of("Usage: " + command.getDescription());
    }

    if (tokens.length == 1) {
      processBuilder.command("fortune");
    } else {
      processBuilder.command("fortune", tokens[1]);
    }

    try {
      Process proc = processBuilder.start();
      int errno = proc.waitFor();
      if (errno != 0) {
        logger.error(
            "got non-zero errno {} when running {}",
            errno,
            String.join(" ", processBuilder.getCommand()));
        return Optional.of("error: " + errno);
      }

      return Optional.of(new String(proc.getInputStream().readAllBytes()));
    } catch (IOException | InterruptedException e) {
      logger.error("I/O exception when running fortune: {}", e.getLocalizedMessage());
      return Optional.of("error: " + e.getLocalizedMessage());
    }
  }
}
