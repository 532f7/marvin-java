package plugins;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import net.qali.plugins.Fortune;
import net.qali.plugins.ProcessBuilderWrapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.telegram.telegrambots.meta.api.objects.Message;

public class FortuneTest {
  @Test
  @DisplayName("reject messages for other commands")
  void shouldHandle_otherCommand() {
    Fortune fortune = new Fortune(null);
    Message msg = Mockito.mock(Message.class);
    when(msg.getText()).thenReturn("/weather toronto");

    boolean actual = fortune.shouldHandle(msg);
    assertFalse(actual);
  }

  @Test
  @DisplayName("Print usage when too many tokens are present")
  void handleMessage_extraToken() {
    Fortune fortune = new Fortune(null);
    Message msg = Mockito.mock(Message.class);
    when(msg.getText()).thenReturn("/fortune foo bar");

    String expected = "Usage: fortune [fortune file]";
    Optional<String> actual = fortune.handleMessage(msg);
    assertTrue(actual.isPresent());
    assertEquals(expected, actual.get());
  }

  @Test
  @DisplayName("Success")
  void handleMessage_success() throws InterruptedException, IOException {
    ProcessBuilderWrapper wrapper = Mockito.mock(ProcessBuilderWrapper.class);
    doNothing().when(wrapper).command(any());

    Process processMock = Mockito.mock(Process.class);
    when(processMock.waitFor()).thenReturn(0);
    String expectedMsg = "test fortune";
    InputStream inputStream = new ByteArrayInputStream(expectedMsg.getBytes());
    when(processMock.getInputStream()).thenReturn(inputStream);
    when(wrapper.start()).thenReturn(processMock);

    Fortune fortune = new Fortune(wrapper);
    Message msg = Mockito.mock(Message.class);
    when(msg.getText()).thenReturn("/fortune cookie");

    Optional<String> actual = fortune.handleMessage(msg);
    assertTrue(actual.isPresent());
    assertEquals(expectedMsg, actual.get());
  }
}
