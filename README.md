# Marvin

A Telegram bot.

## Setup

Message `@BotFather` on Telegram to create a bot. You'll receive a bot token after creating your bot:

>Use this token to access the HTTP API:
>TOKEN_IS:HERE_TOKEN
>Keep your token secure and store it safely, it can be used by anyone to control your bot.

Start the bot by specifying a webhook callback property and bot token:

    java -jar marvin-java-1.0.jar -Dtoken=TOKEN_IS:HERE_TOKEN